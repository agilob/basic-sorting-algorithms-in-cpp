#include "header/sorting.h"


/**
 * O( N log(N))
 * 
 * 5, 2, 56, 15, 7, 8, 5, 1, 4, 10, 11, 14
 * Number of changes: 5273
 * 
 * 2, 1, 5, 5, 7, 8, 15, 17, 20, 22, 32, 14
 * Number of changes: 802
 * 
 * @param unSorted
 * @param left
 * @param right
 * @param counter
 * @return 
 */
int quickSort(std::vector<int>& unSorted, int left, int right, int counter) {

    if (left < right) {
        int m = left;

        for (int i = left + 1; i <= right; i++)
            if (unSorted.at(i) < unSorted.at(left)) {
                std::swap(unSorted.at(++m), unSorted.at(i));
                counter++;
            } //if

        std::swap(unSorted.at(left), unSorted.at(m));

        counter += quickSort(unSorted, left, m - 1, counter);
        counter += quickSort(unSorted, m + 1, right, counter);
    } //for 
    return counter;
} //quickSort
