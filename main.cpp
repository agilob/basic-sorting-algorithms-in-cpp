#include <time.h>
#include <random>
#include <iostream>
#include <limits.h>
#include <algorithm>

#include "header/sorting.h"

void printer(std::vector<int> &unsorted);

void randomize(std::vector<int> &unsorted, unsigned int SIZE) {

    std::random_device dev;

    for (int i = 0; i < SIZE; i++) {

        dev.max();
        dev.max();
        dev.entropy();

        srand(rand() + time(0));

        unsorted.push_back(abs(static_cast<int> ((dev()) * (rand() * rand()))) % 965);

    }
}

double diffclock(clock_t start) {
    clock_t stop = clock();

    double diffticks = start - stop;

    double diffms = abs((diffticks) / (CLOCKS_PER_SEC / 1000));

    std::cout << "Time taken in ms: " << diffms << std::endl;
    std::cout << "Time taken in sec: " << diffms / 1000 << std::endl << std::endl;

}

void printer(std::vector<int> &unsorted) {
    for (auto vec : unsorted) {
        std::cout << vec << " ";
    }
    std::cout << std::endl;
}

int main(int argc, char** argv) {

    clock_t start_time;
    unsigned int SIZE = 10000;
    std::vector<int> unsorted;


    randomize(unsorted, SIZE);

    std::cout << "Heap sort" << std::endl;
    start_time = clock();
    heapSort(unsorted);
    diffclock(start_time);


    randomize(unsorted, SIZE);

    std::cout << "Insertion sort" << std::endl;
    start_time = clock();
    insertionSort(unsorted, unsorted.size() - 1);
    diffclock(start_time);


    randomize(unsorted, SIZE);

    std::cout << "Merge sort" << std::endl;
    start_time = clock();
    mergeSort(unsorted);
    diffclock(start_time);


    randomize(unsorted, SIZE);

    std::cout << "Shaker sort" << std::endl;
    start_time = clock();
    shakerSort(unsorted, unsorted.size() - 1);
    diffclock(start_time);


    randomize(unsorted, SIZE);

    std::cout << "Bubble sort" << std::endl;
    start_time = clock();
    bubbleSort(unsorted, unsorted.size() - 1);
    diffclock(start_time);
//

    unsorted.clear();
    return 0;
}

