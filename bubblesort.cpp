#include "header/sorting.h"

/**
 * Expensive, O(N^2). Sometimes one change in array may result in more iterations
 * in the main loop:
 * 1, 0, 5, 5, 7, 8, 15, 17, 20, 22, 32 - number of changes 1
 * 1, 5, 2, 56, 2, 15, 7, 8, 5, 1 - number of changes 30
 */
void bubbleSort(std::vector<int>& unSorted, int size) {

    int tmp;
    int counter = 0;

    for (int i = 1; i < size; i++) { //for each element in the array
        for (int j = size - 1; j >= i; j--) { //go backward until current element
            if (unSorted.at(j) < unSorted.at(j - 1)) { //if `next` (previous) element is
                //bigger then current
                tmp = unSorted.at(j - 1);
                unSorted.at(j - 1) = unSorted.at(j);
                unSorted.at(j) = tmp;
                counter++;
            } //if
        } //2nd for
    } //1st for
    std::cout << "Number of changes: " << counter << std::endl;

} //bubble sort
