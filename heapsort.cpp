#include "header/sorting.h"
#include <algorithm>
#include <limits.h>

std::vector<int> heapSort(std::vector<int>& unSorted) {

    std::vector<int> sorted;

    int min = INT_MAX,
            position;

    std::vector<int>::iterator pos;

    for (auto elem : unSorted) {
        min = INT_MAX;

        for (std::vector<int>::iterator i = unSorted.begin();
                i != unSorted.end(); ++i) {

            if (min > *i) {
                min = *i;
                pos = i;
            }

        }

        sorted.push_back(min);
        unSorted.erase(pos); //this element is no longer needed
    }
    return sorted;
}