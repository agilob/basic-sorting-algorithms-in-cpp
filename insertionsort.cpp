#include "header/sorting.h"

/**
 * Expensive, O(N^2), code is short and easy to understand.
 * 
 * 1, 5, 2, 56, 2, 15, 7, 8, 5, 1
 * Number of changes: 65565
 * 
 * 1, 0, 5, 5, 7, 8, 15, 17, 20, 22, 32
 * Number of changes: 65536
 */
void insertionSort(std::vector<int>& unSorted, int size) {
    int j,
            tmp,
            counter = 0;

    for (int i = 0; i < size; i++) {
        j = i;
        tmp = unSorted[j];

        while (j > 0 && unSorted[j - 1] > tmp) {
            unSorted[j] = unSorted[j - 1];
            j--;
            counter++;
        } //while

        unSorted[j] = tmp;
    } //for
    std::cout << "Number of changes: " << counter << std::endl;
} //insertion sort
