#include "header/sorting.h"

/**
 * Shaker sort is a "better" implementation of bubble sort. It's still O(N^2),
 * but implementation is a bit faster.
 * 
 * 2, 1, 5, 5, 7, 8, 15, 17, 20, 22, 32
 * Number of changes: 12
 * 
 * 5, 2, 56, 15, 7, 8, 5, 1, 4, 10, 11
 * Number of changes: 38
 * 
 */
void shakerSort(std::vector<int>& unSorted, int size) {

    int left = 1,
            right = size - 1,
            k = size - 1,
            counter = 0;
    int j;

    do {
        for (j = right; j >= left; j--) {
            if (unSorted.at(j - 1) > unSorted.at(j)) {
                std::swap(unSorted.at(j - 1), unSorted.at(j));
                k = j;
                counter++;
            } //if
        }
        left = k + 1;
        for (j = left; j <= right; j++) {
            if (unSorted.at(j - 1) > unSorted.at(j)) {
                std::swap(unSorted.at(j - 1), unSorted.at(j));
                k = j;
                counter++;
            } //if
        }//for j = right

        right = k - 1;
    } while (left < right);

    std::cout << "Number of changes: " << counter << std::endl;

} //shaker sort
