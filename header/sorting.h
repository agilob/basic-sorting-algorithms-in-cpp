#include <vector>
#include <iostream>

#ifndef SORTING_H
#define	SORTING_H

void bubbleSort(std::vector<int>& unSorted, int size);
std::vector<int> heapSort(std::vector<int>& unSorted);
void insertionSort(std::vector<int>& unSorted, int size);
std::vector<int> merge(const std::vector<int>& left, const std::vector<int>& right);
std::vector<int> mergeSort(std::vector<int>& unSorted);
int quickSort(std::vector<int>& unSorted, int left, int right, int counter);
void shakerSort(std::vector<int>& unSorted, int size);

#endif	/* SORTING_H */

