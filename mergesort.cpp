#include "header/sorting.h"

std::vector<int> merge(const std::vector<int>& left, const std::vector<int>& right) {

    // Fill the resultant vector with sorted results from both vectors
    std::vector<int> result;
    unsigned left_it = 0, right_it = 0;

    while (left_it < left.size() && right_it < right.size()) {
        // If the left value is smaller than the right it goes next
        // into the resultant vector
        if (left[left_it] < right[right_it]) {
            result.push_back(left[left_it]);
            left_it++;
        } else {
            result.push_back(right[right_it]);
            right_it++;
        }
    }

    while (left_it < left.size()) {
        result.push_back(left[left_it]);
        left_it++;
    }

    while (right_it < right.size()) {
        result.push_back(right[right_it]);
        right_it++;
    }

    return result;
}

std::vector<int> mergeSort(std::vector<int>& unSorted) {

    if (unSorted.size() == 1) {
        return unSorted;
    }

    std::vector<int>::iterator middle = unSorted.begin() + (unSorted.size() / 2);

    std::vector<int> left(unSorted.begin(), middle);
    std::vector<int> right(middle, unSorted.end());

    left = mergeSort(left);
    right = mergeSort(right);

    return merge(left, right);
}
